#!/usr/bin/env groovy
import groovy.json.JsonBuilder
import groovy.json.JsonSlurperClassic
import hudson.tasks.test.AbstractTestResultAction

def lambda_configs = new ArrayList<Object>()
def libs_configs = new ArrayList<Object>()
def docker_configs = new ArrayList<Object>()
def layer_configs = new ArrayList<Object>()
String S3bucket_USWest2 = "alts-aws-service-utils"
String S3bucket2_USEast1 = "fokal-devops-us-east-1"
String S3bucket3_USEast2 = "test-jenkins-dedicated-buck"
String path_artifacts_wheels = "jenkins_test/wheels"
String path_artifacts_lambdas = "jenkins_test/lambdas"
String path_artifacts_layers = "jenkins_test/layers"
String path_artifacts_misc = "jenkins_test/misc"
String path_to_avoid = ""
String runType = 'DEV'
String threadID = ""
String teamdomain = "fokal-ai"
String slackToken = 'slack_token'
Map<String, String> regionBucketMap = new HashMap<String, String>()
pipRepoHostName = ""
lambdaArtifactsDirName = "lambda_artifacts"
slackChannelName = "jenkins"
workspace = ""
baseWorkspace = ""
def failed_files = ""

class TestReport {
    private int total;
    private int failed;
    private int passed;
    private int skipped;

    int getSkipped() {
        return skipped
    }

    void setSkipped(int skipped) {
        this.skipped = skipped
    }

    int getTotal() {
        return total
    }

    void setTotal(int total) {
        this.total = total
    }

    int getFailed() {
        return failed
    }

    void setFailed(int failed) {
        this.failed = failed
    }

    int getPassed() {
        return passed
    }

    void setPassed(int passed) {
        this.passed = passed
    }
}
def AddExtraPythonPath(String path,base_directory){
    
path = path + ":${base_directory}/alts_gsuite_utils/alts_gsuite_utils"+":${base_directory}/alts_aws_utils/alts_aws_utils" + ":${base_directory}/alts_core_utils/alts_core_utils" + 
    ":${base_directory}/alts_db_utils/alts_db_utils2" + ":${base_directory}/alts_elastic_search_utils/alts_elastic_search_utils" + ":${base_directory}/alts_gsheet_utils/alts_gsheet_utils" + 
    ":${base_directory}/alts_pdf_utils/alts_pdf_utils" + ":${base_directory}/alts_slack_utils/alts_slack_utils" + ":${base_directory}/alts_tableau_utils/alts_tableau_utils" + 
    ":${base_directory}/deprecated_utils/alts_slack_utils_v2" + ":${base_directory}/deprecated_utils/alts_db_utils";
    return path
}
def buildPythonPathsFromLibConfig(libs_configs, base_directory) {
    
    List<String> pack = new ArrayList<String>();
    for (libs_config in libs_configs) {
        pack.add("${base_directory}/${libs_config.dir_path}")
    }
    String path = pack.join(":");
    
    path = AddExtraPythonPath(path,base_directory)
    println(path);
    return path
}
def pipInstallFromRequirements(String requirementsFileName, String destinationPath, boolean onlyBinaries=false) {
//--only-binary=:all:
    sh 'ls'
    sh """pip install --trusted-host ${
        pipRepoHostName
    } --extra-index-url=https://pypi.org/simple -i simple -r ${requirementsFileName} -t ${destinationPath} >/dev/null"""
}
def getTestReport() {
    TestReport testReport = null
    def testResult = build.testResultAction
    AbstractTestResultAction testResultAction = currentBuild.rawBuild.getAction(AbstractTestResultAction.class)
    if (testResultAction != null) {
        println('&()*')
        testReport = new TestReport()
        total = testResultAction.totalCount
        failed = testResultAction.failCount
        skipped = testResultAction.skipCount
        testReport.setTotal(total)
        testReport.setFailed(failed)
        testReport.setSkipped(skipped)
        def passed = total - failed - skipped
        testReport.setPassed(passed)

        println "Test Status:\n  Passed: ${testReport.getPassed()}, Failed: ${testReport.getFailed()}, Skipped: ${testReport.getSkipped()}"
    }
    return testReport
}
pipeline {
    agent any 
    stages 
    {
    stage("unit_tests") {
            steps {
                checkout scm
                sh 'ls'
                script {
                    workspace = pwd()
                    dir("${workspace}/test_packages") {
                        String requirementsFilePath = "${workspace}/requirements.txt"
                        String destinationPath = "${workspace}/test_packages/"
                        sh 'pip3 install pytest pytest-cov slack slackclient tableaudocumentapi PyPDF2'
                        sh 'pip3 install nameparser==1.0.2 mock elasticsearch pytesseract tableauserverclient nose googlemaps'
                        sh 'pip3 install dateparser==0.7.1 pdfminer'
                        sh 'pip3 install requests==2.18.4'
                        sh 'pip3 install psycopg2-binary'
                        sh 'pip3 install email_validator==1.0.3 pandas'
                        sh 'pip3 install SQLAlchemy==1.3.2'
                        sh 'pip3 install boto3==1.9.128 httplib2==0.12.0 apiclient==1.0.4 google-api-python-client==1.7.8'
                        sh 'pip3 install google-auth-oauthlib==0.2.0 oauth2client==4.1.3 oauthlib==3.0.1 slackclient==1.3.2 psycopg2-binary rarfile grequests'                        
                        //pipInstallFromRequirements(requirementsFilePath,destinationPath)
                    }
                    if (fileExists("libs_config.json")) {
                            def inputFile = readFile("libs_config.json")
                            libs_configs = new groovy.json.JsonSlurperClassic().parseText(inputFile)
                    }
                    println(libs_configs)
                    def pythonPaths = buildPythonPathsFromLibConfig(libs_configs, workspace)
                    println(pythonPaths);
                    sh 'ls'
                    dir("${workspace}") {
                        sh 'ls'
                        writeFile file: ".coveragerc", text: """[run] omit=**/test_packages/*"""
                        def exitCode = sh script: """
                        PYTHONPATH=${env.PYTHONPATH}:${workspace}/test_packages:${
                            pythonPaths
                        } python3 -m pytest --junitxml=report.xml -o junit_family=xunit2 --cov-config=${
                            workspace
                        }/.coveragerc --cov=${
                            workspace
                        } --cov-report=xml --continue-on-collection-errors -rf | tee log.txt
                        """, returnStatus: true
                        failed_files = sh returnStdout: true, script: "grep -E 'FAILED' log.txt"
                        println("The failed files are - ${failed_files}")
                        if (fileExists("${workspace}/coverage.xml")) {
                            cobertura autoUpdateHealth: false, autoUpdateStability: false, coberturaReportFile: 'coverage.xml', conditionalCoverageTargets: '70, 0, 0', failUnhealthy: false, failUnstable: false, lineCoverageTargets: '80, 0, 0', maxNumberOfBuilds: 0, methodCoverageTargets: '80, 0, 0', onlyStable: false, sourceEncoding: 'ASCII', zoomCoverageChart: false
                            xunit([JUnit(deleteOutputFiles: true, failIfNotNew: true, pattern: '**/coverage.xml', skipNoTestFiles: false, stopProcessingIfError: false)])
                        }
                    }

                    TestReport testReport = getTestReport()
                    if (testReport) {
                        if (testReport.getTotal() == 0 )
                        {
                            testmessage = "No unit tests were executed."
                        }
                        else if(testReport.getPassed() == testReport.getTotal())
                        {
                            testmessage = "All the unit tests were executed." + " ${testReport.getTotal()}/${testReport.getTotal()} "
                        }
                        else{
                            testmessage = "Some unit tests were failed" + " ${testReport.getFailed()}/${testReport.getTotal()} " + "and skipped unit tests were " + " ${testReport.getSkipped()}/${testReport.getTotal()} "
                            }
                        }
                        //else 
                        //{
                        //    println("Some error ocurred while pytesting")
                        //}
                        //testmessage = "Test Status:\n  Passed: ${testReport.getPassed()}, Failed: ${testReport.getFailed()}, Skipped: ${testReport.getSkipped()}"
                        //println(${testReport.getPassed()});
                        //println(${testReport.getFailed()});
                        //println(${testReport.getSkipped()});
                       // notifyBuild(testmessage, false, threadID, teamdomain, slackToken)
                    }
                }
            }
        }
}